class Hewan5 {
    constructor(public nama: string, public kaki: number){}
}

class Katak extends Hewan5 {
    constructor(
        nama: string,
        kaki: number,
        public beracun: boolean
    ) {
        super(nama, kaki);
    }
}

const katak = new Katak('Katak', 2, true);
console.log(katak);
