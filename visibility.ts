//public
//protected
//private

class Hewan7 {
    public nama: string;
    private kaki: number;
    protected mamalia: boolean;

    constructor(nama: string, kaki: number, mamalia: boolean) {
        this.nama = nama;
        this.kaki = kaki;
        this.mamalia = mamalia;
    }

    berjalan() {
        //bisa akses semua
        this.nama;
        this.kaki;
        this.mamalia;
    }
}

class Katak7 extends Hewan7 {
    private umurTelur: number = 4;
    private umurKecebong: number = 7;
    private umurKatak: number = 90;

    getUmur() {
        console.log(this.umurKatak + this.umurKecebong + this.umurTelur);
        
        //protected masih bisa dipanggil disini ( kelas turunannya )
        this.mamalia;

        //private gabisa disini
        //this.kaki;
    }
}

const katak7 = new Katak7("katak", 4, true);
katak7.getUmur();