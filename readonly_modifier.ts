class Person {
    //dipakai di property
    readonly gender: string = "Pria";
}

//dipakai di variabel biasa
const person = new Person();

//error gabisa diubah
person.gender = "Wanita";

// readonly vs const
// keduanya sama2 gabisa diubah
// readonly dipakai buat property
// const dipakai buat variabel biasa

