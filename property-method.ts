class Hewan3 {

    //property (variabel didalam kelas)
    nama: string = '';
    kaki: number = 0;
    mamalia: boolean = false;

    //method (function didalam kelas)
    bernafas() {
        console.log(`${this.nama} sedang bernafas`);
    }
}

const hewan3 = new Hewan3();

hewan3.nama = 'katak';
console.log(hewan3);

hewan3.bernafas();
