class Hewan42 {
    nama: string = '';
    kaki: number = 0;

    bernafas() {
        console.log("bernafas");
    }
}

class Burung42 extends Hewan42 {
    warna: string = "merah";
}

const burungs = new Burung42();
burungs.nama = "kenari";
burungs.kaki = 2;
burungs.warna = "kuning";

console.log(burungs);
burungs.bernafas();
